package de.kkickstein.testapplication.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose


/**
 * Created by kickstein on 27.03.18.
 */

@Entity(tableName = "data_item")
class DataItem {
  @Expose var no: Int? = null
  @Expose var name: String? = null
  @Expose var price: Double? = null
  @Expose var expires: String? = null
  @PrimaryKey  var creationTime: Long = System.currentTimeMillis()
}