package de.kkickstein.testapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import de.kkickstein.testapplication.repo.getAllDataResponses

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    Thread({getAllDataResponses {  }}).start()
  }
}
