package de.kkickstein.testapplication.repo

import de.kkickstein.testapplication.App
import de.kkickstein.testapplication.entities.DataResponse
import de.kkickstein.testapplication.networking.getTestData
import de.kkickstein.testapplication.storage.getAllDataResponsesFromDB
import de.kkickstein.testapplication.storage.saveToDB

/**
 * Created by kickstein on 27.03.18.
 */

fun getAllDataResponses(dataAction: (List<DataResponse>) -> Unit) {
  val dbDataResponses = getAllDataResponsesFromDB()
  if (dbDataResponses != null && !dbDataResponses.isEmpty()) {
    dataAction(dbDataResponses)
  } else {
    getTestData(App.applicationContext(), {
      it.let {
        dataAction.invoke(listOf(it))
        Thread({saveToDB(it)}).start()
      }
    }, {
      dataAction(emptyList())
    })
  }
}

