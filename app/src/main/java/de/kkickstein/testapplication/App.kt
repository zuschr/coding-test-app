package de.kkickstein.testapplication

import android.app.Application
import android.content.Context

/**
 * Created by kickstein on 27.03.18.
 */
class App : Application() {

  init {
    instance = this
  }

  companion object {
    private var instance: App? = null

    fun applicationContext() : Context {
      return instance!!.applicationContext
    }
  }
}