package de.kkickstein.testapplication.storage.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import de.kkickstein.testapplication.entities.DataResponse

/**
 * Created by kickstein on 27.03.18.
 */

@Dao
interface DataResponseDao {
  @Query("SELECT * from data_response")
  fun getAll(): List<DataResponse>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insert(item: DataResponse)

  @Delete
  fun delete(item: DataResponse)


  @Query("DELETE from data_response")
  fun deleteAll()
}