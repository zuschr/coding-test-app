package de.kkickstein.testapplication.storage

import de.kkickstein.testapplication.App
import de.kkickstein.testapplication.entities.DataItem
import de.kkickstein.testapplication.entities.DataResponse
import de.kkickstein.testapplication.storage.impl.AppDatabase

/**
 * Created by kickstein on 27.03.18.
 */

private val _database = AppDatabase.getInstance(App.applicationContext())

fun saveToDB(item:DataItem) {
  _database?.dataItemDao()?.insert(item)
}

fun saveToDB(item:DataResponse) {
  _database?.dataResponseDao()?.insert(item)
}

fun deleteFromDB(item:DataItem) {
  _database?.dataItemDao()?.delete(item)
}

fun deleteFromDB(item:DataResponse) {
  _database?.dataResponseDao()?.delete(item)
}

fun getAllDataResponsesFromDB() : List<DataResponse>? {
  return _database?.dataResponseDao()?.getAll()
}

fun getAllDataItemsFromDB() : List<DataItem>? {
  return _database?.dataItemDao()?.getAll()
}