package de.kkickstein.testapplication.storage.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import de.kkickstein.testapplication.entities.DataItem



/**
 * Created by kickstein on 27.03.18.
 */

@Dao
interface DataItemDao {
  @Query("SELECT * from data_item")
  fun getAll(): List<DataItem>

  @Insert(onConflict = REPLACE)
  fun insert(item: DataItem)

  @Delete
  fun delete(item: DataItem)


  @Query("DELETE from data_item")
  fun deleteAll()
}