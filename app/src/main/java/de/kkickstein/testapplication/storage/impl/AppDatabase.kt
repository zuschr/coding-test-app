package de.kkickstein.testapplication.storage.impl

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import de.kkickstein.testapplication.entities.DataItem
import de.kkickstein.testapplication.entities.DataResponse
import de.kkickstein.testapplication.storage.dao.DataItemDao
import de.kkickstein.testapplication.storage.dao.DataResponseDao

/**
 * Created by kickstein on 27.03.18.
 */

@Database(entities = arrayOf(DataResponse::class, DataItem::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

  abstract fun dataItemDao(): DataItemDao
  abstract fun dataResponseDao(): DataResponseDao

  companion object {
    private var _instance: AppDatabase? = null

    fun getInstance(context: Context): AppDatabase? {
      if (_instance == null) {
        synchronized(AppDatabase::class) {
          _instance = Room.databaseBuilder(context.getApplicationContext(),
              AppDatabase::class.java, "database.db")
              .build()
        }
      }
      return _instance
    }

    fun destroyInstance() {
      _instance = null
    }
  }
}