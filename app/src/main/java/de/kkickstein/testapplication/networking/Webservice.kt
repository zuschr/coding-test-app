package de.kkickstein.testapplication.networking

import android.content.Context
import com.google.gson.Gson
import de.kkickstein.testapplication.entities.DataResponse
import de.kkickstein.testapplication.networking.impl.VolleyWebservice

/**
 * Created by kickstein on 27.03.18.
 */

private val _testDataUrl = "http://pastebin.com/raw/U8B5EKUd"
private val _gson = Gson()

fun getTestData(context: Context?,
    dataAction: (data : DataResponse) -> Unit, errorAction: (error: Throwable) -> Unit) {
  doWebServiceRequest(context = context, url = _testDataUrl,
                      onSuccess = dataAction, onError = errorAction)
}

private inline fun <reified T> doWebServiceRequest(context: Context?, url: String,
    crossinline onSuccess:(response: T) -> Unit,
    crossinline onError:(error: Throwable) -> Unit) {
  context.let {
    val webserviceImpl = VolleyWebservice(context!!)
    webserviceImpl.doGetRequest(url, {
      try {
        onSuccess(_gson.fromJson(it, T::class.java))
      } catch (e: Exception) {
        onError(e)
      }
    }, { error -> Exception(error.message) })
  }
}