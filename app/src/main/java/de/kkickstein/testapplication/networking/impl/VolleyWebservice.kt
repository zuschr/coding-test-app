package de.kkickstein.testapplication.networking.impl

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley

/**
 * Created by kickstein on 27.03.18.
 */


class VolleyWebservice constructor(context: Context) {
  private val queue : RequestQueue = Volley.newRequestQueue(context)

  fun doGetRequest(url: String,
      onSuccess: (response: String) -> Unit, onError: (error: VolleyError) -> Unit) {
    val stringRequest = StringRequest(Request.Method.GET, url,
        Response.Listener<String> { response ->
          onSuccess(response);
        },
        Response.ErrorListener {
          onError(it)
        })

    queue.add(stringRequest)
  }
}